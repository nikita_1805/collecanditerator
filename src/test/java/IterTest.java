import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.*;

public class IterTest {
    private IndexedList<Integer> testList;
    private Iter<Integer> iterator;
    @Before
    public void setUp(){
        this.testList = new IndexedList<>(5);
        for(int i = 0; i < this.testList.arraySize();i++){
            this.testList.addElement(i);
        }
    }

    @Test
    public void isEmpty() {
        Iter<Integer> iterator = new Iter<>(testList);

        assertFalse(iterator.isEmpty());
    }

    @Test
    public void nextElem() {
        Iter<Integer> iterator = new Iter<>(testList);
        int check = (int)iterator.nextElem();

        assertNotNull(check);
    }

    @Test
    public void hasNextElement() {
        Iter<Integer> iterator = new Iter<>(testList);
        boolean test;
        int check = (int)iterator.nextElem();
        check = (int)iterator.nextElem();
        check = (int)iterator.nextElem();
        test = iterator.hasNextElement();

        assertTrue(test);

        check = (int)iterator.nextElem();
        check = (int)iterator.nextElem();
        test = iterator.hasNextElement();

        assertFalse(test);
    }
}