import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IndexedListTest {
    private IndexedList list;
    private final int TEST_VALUE = 5;
    private final int testElement = 4;

    @Before
    public void setUp() {
        list = new IndexedList<>(5);

        for (int i = 0; i < list.arraySize(); i++) {
            list.addElement(i);
        }
    }

    @Test
    public void addElement() {
        IndexedList testList = new IndexedList();
        testList.addElement(4);

        assertEquals(4, testList.getElement(0));
    }

   /* @Test
    public void deleteElement() {
        Integer[] list = {0, 1, 2, 3, 4};
        this.list.deleteElement(2);

        assertEquals(3, this.list.getElement(2));
    }

    @Test
    public void deleteElement1() {
        Integer list[] = {0, 1, 2, 2, 4};
        this.list.deleteElement((Object) 2);

        assertEquals(4, this.list.getElement(4));
    }*/

    @Test
    public void getElement() {
        assertEquals(testElement, this.list.getElement(4));
    }

    @Test
    public void arraySize() {
        int sze = list.arraySize();
        assertEquals("Proper size", TEST_VALUE, sze);
    }

}