import CollectionInterfaces.Iterator;

public class Iter<T> implements Iterator<T> {
    private int first;
    private IndexedList<T> linkList;
    public Iter(IndexedList<T> list){
        this.first = 0;
        this.linkList = list;
    }

    public boolean isEmpty(){
        return this.linkList.getElement(this.first) == null;
    }
    @Override
    public Object nextElem() {
        Object nextElem = this.linkList.getElement(this.first);
        if (first != this.linkList.arraySize())
        first+=1;
        return nextElem;
    }
    @Override
    public boolean hasNextElement() {
        int check = this.first;
        return ++check <= this.linkList.arraySize();
    }

}
