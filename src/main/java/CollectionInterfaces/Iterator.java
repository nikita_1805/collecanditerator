package CollectionInterfaces;

public interface Iterator <T> {
    Object nextElem();

    boolean hasNextElement();
}
