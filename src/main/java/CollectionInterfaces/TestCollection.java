package CollectionInterfaces;

public interface TestCollection <E> {
    void addElement( E element);
    void deleteElement(E element);
    void deleteElement(int index);
    E getElement(int index);
    int arraySize();
}
