import CollectionInterfaces.TestCollection;

import java.util.Iterator;

public class IndexedList<T> implements TestCollection<T> {
    protected Object[] list;
    private int size;


    public IndexedList(int capacity) {
        this.list = new Object[capacity];
        this.size = 0;
    }

    public IndexedList() {
        this.list = new Object[10];
        this.size = 0;
    }

    private void ensureCapacity(int index) {
        if (list.length < size) {
            Object[] newArray = new Object[(int) (list.length * 1.5)];
            System.arraycopy(list, 0, newArray, 0, list.length);
            list = newArray;
        }
    }

    @SuppressWarnings("unchecked")
    public void addElement(T element) {
        int newItemPosition = size + 1;
        ensureCapacity(newItemPosition);
        list[size] = element;
        size++;
    }

    @Override
    public void deleteElement(int index) {
        int check = 0;
        for (int i = 0; i < this.list.length; i++) {
            if (i == index) {
                this.list[i] = null;
                check = 2;
            }
        }
        if (check != 0) {
            for (int i = index; i < this.list.length; i++) {
                if (i == (this.list.length - 1)) break;
                this.list[i] = this.list[i + 1];
            }
        }
        // this.list[list.length-1] = null;
    }

    @Override
    public void deleteElement(Object element) {
        for (int i = 0; i < this.list.length; i++) {
            if (this.list[i].equals(element)) {
                list[i] = null;
            }
        }
        for (int i = 0; i < this.list.length; i++) {
            int check = 0;
            if (this.list[i] == null) {
                check = 1;
            }
            if (check == 1) {
                if (i == (this.list.length - 1)) break;
                this.list[i] = this.list[i + 1];
            }
        }
    }

    @Override
    public T getElement(int index) {

        if (index > list.length) {
            throw new IndexOutOfBoundsException();
        }

        Object o = list[index];
        return (T) o;
    }

    @Override
    public int arraySize() {
        return this.list.length;
    }

    public Iterator<T> iterator() {
        return new Iter<>();
    }

    private class Iter<Type> implements Iterator<Type> {
        private int first;

        public Iter() {
            this.first = 0;
        }

        @Override
        public boolean hasNext() {
            return first != size;
        }

        @Override
        public Type next() {
            Type element = (Type) IndexedList.this.getElement(first);
            first++;
            return element;
        }
    }

}
